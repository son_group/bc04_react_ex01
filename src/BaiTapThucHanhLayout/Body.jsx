import React, { Component } from "react";
import Banner from "./Banner";
import ItemDetail from "./ItemDetail";

export default class Body extends Component {
  render() {
    return (
      <div>
        <Banner />
        <section className="pt-4">
          <div className="container px-lg-5">
            <div className="row gx-lg-5">
              <ItemDetail />
              <ItemDetail />
              <ItemDetail />
              <ItemDetail />
            </div>
          </div>
        </section>
      </div>
    );
  }
}
