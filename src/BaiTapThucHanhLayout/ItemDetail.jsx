import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div className="card col-3 mb-5 border-0">
        <div className="card-body border">
          <img
            src="http://loremflickr.com/640/480/technics"
            className="card-img-top"
            alt="..."
          />
          <h5 className="card-title">Card title</h5>
          <p className="card-text">
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <a href="#" className="btn btn-primary">
            Go somewhere
          </a>
        </div>
      </div>
    );
  }
}
